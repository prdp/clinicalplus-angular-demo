import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../../../services/customers/customers.service';
import { ActivatedRoute, Router } from '@angular/router';

import { ITdDataTableColumn } from '@covalent/core/data-table';
import { ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';

const DECIMAL_FORMAT: (v: any) => any = (v: number) => v.toFixed(2);

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss'],
  providers: [DatePipe]
})

export class CustomerDetailsComponent implements OnInit {
  customer: any;
  customerOrders: any;
  changeDetectorRef: ChangeDetectorRef;

  columns: ITdDataTableColumn[] = [
    { name: 'Order.Id',  label: 'Order Id' },
    { name: 'Order.OrderDate', label: 'Order date' },
    { name: 'Order.RequiredDate', label: 'Required date' },
    { name: 'Order.ShippedDate', label: 'Shipping date' },
    { name: 'Order.Freight', label: 'Freight' },
    { name: 'Order.ShipAddress', label: 'Shipping address' },
    { name: 'Order.ShipCity', label: 'City' },
  ];

  constructor(
    private _customers: CustomersService,
    private route: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef,
    private datePipe: DatePipe,
    private router: Router
  ) {
    this.changeDetectorRef = changeDetectorRef;
  }

  ngOnInit() {
    this.getCustomerOrderDetails(this.route.snapshot.params['id']);
  }

  goTodetails(e) {
    this.router.navigate(['/orders', e.row.Order.Id]);
  }

  getCustomerOrderDetails(id) {
    this._customers.customerDetails('customers', id).subscribe((result: any) => {
      this.customer = result.Customer;
      this.customerOrders = result.CustomerOrders.map(this.formatOrder);
      this.changeDetectorRef.detectChanges();
    });
  }

  private formatOrder = order => {
    const requiredDate = parseInt(order.Order.RequiredDate.substr(6), 10);
    const orderDate = parseInt(order.Order.OrderDate.substr(6), 10);
    const shippedDate = parseInt(order.Order.ShippedDate.substr(6), 10);
    order.Order.RequiredDate = this.datePipe.transform(requiredDate, 'mediumDate');
    order.Order.OrderDate = this.datePipe.transform(orderDate, 'mediumDate');
    order.Order.ShippedDate = this.datePipe.transform(shippedDate, 'mediumDate');
    return order;
  }
}
