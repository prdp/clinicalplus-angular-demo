import { Component, OnInit} from '@angular/core';
import { TdDataTableService, TdDataTableSortingOrder, ITdDataTableSortChangeEvent, ITdDataTableColumn } from '@covalent/core/data-table';
import { IPageChangeEvent } from '@covalent/core/paging';
import { CustomersService } from '../../services/customers/customers.service';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  changeDetectorRef: ChangeDetectorRef;
  columns: ITdDataTableColumn[] = [
    { name: 'Id', label: 'Id', sortable: true, width: 70 },
    { name: 'ContactName',  label: 'Contact Name', sortable: true, width: 250 },
    { name: 'CompanyName', label: 'Company Name', sortable: true, width: 150 },
    { name: 'ContactTitle', label: 'Contact Title', hidden: false },
    { name: 'Phone', label: 'Phone', numeric: true , width: 200 },
    { name: 'Address', label: 'Address', sortable: true, width: 200 },
    { name: 'City',  label: 'City', sortable: true, width: 100 },
    { name: 'Fax', label: 'Fax', sortable: true, width: 200 },
    { name: 'PostalCode', label: 'Postal Code', hidden: false },
    { name: 'Country', label: 'Country', sortable: true, width: 150 },
  ];
  data: any;
  filteredData: any[];
  filteredTotal: number;
  searchTerm = '';
  fromRow = 1;
  currentPage = 1;
  pageSize = 5;
  sortBy = 'Id';
  selectedRows: any[] = [];
  sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;

  constructor(
      private _dataTableService: TdDataTableService,
      private _customers: CustomersService,
      public router: Router,
      changeDetectorRef: ChangeDetectorRef
  ) {
    this.changeDetectorRef = changeDetectorRef;
  }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    this._customers.getCustomers('/query/customers').subscribe(((data: any) => {
      this.data = data.Results;
      this.filteredTotal = data.Results.length;
      this.filter();
    }));
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.filter();
  }

  showAlert(event: any): void {
    const row: any = event.row;
    this.router.navigate(['/customers', row.Id]);
  }

  filter(): void {
    let newData: any[] = this.data;
    const excludedColumns: string[] = this.columns
    .filter((column: ITdDataTableColumn) => {
      return ((column.filter === undefined && column.hidden === true) ||
              (column.filter !== undefined && column.filter === false));
    }).map((column: ITdDataTableColumn) => {
      return column.name;
    });
    newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
    this.filteredTotal = newData.length;
    newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData = newData;
    this.changeDetectorRef.detectChanges();
  }

}
