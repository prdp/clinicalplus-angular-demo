import { Component, OnInit } from '@angular/core';
import { TdDataTableService, TdDataTableSortingOrder, ITdDataTableSortChangeEvent, ITdDataTableColumn } from '@covalent/core/data-table';
import { IPageChangeEvent } from '@covalent/core/paging';
import { OrdersService } from '../../services/orders/orders.service';
import { ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  providers: [DatePipe]
})
export class OrdersComponent implements OnInit {
  data: any;
  filteredData: any[];
  filteredTotal: number;
  searchTerm = '';
  fromRow = 1;
  currentPage = 1;
  pageSize = 5;
  sortBy = 'Id';
  selectedRows: any[] = [];
  sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;
  changeDetectorRef: ChangeDetectorRef;
  columns: ITdDataTableColumn[] = [
    { name: 'Id', label: 'Id', sortable: true, width: 70 },
    { name: 'EmployeeId', label: 'Employee Id', sortable: true },
    { name: 'OrderDate', label: 'Order Date', hidden: false, width: 150 },
    { name: 'ShippedDate', label: 'Shipped Date', hidden: false },
    { name: 'ShipVia',  label: 'Ship Via', sortable: true, width: 100 },
    { name: 'Freight', label: 'Freight', sortable: true, width: 100 },
    { name: 'ShipName', label: 'Ship Name', sortable: true, width: 150 },
    { name: 'ShipAddress', label: 'Ship Address', sortable: true, width: 150 },
    { name: 'ShipCity', label: 'Ship City', sortable: true, width: 150 },
    { name: 'ShipPostalCode', label: 'Ship Postal Code', sortable: true, width: 150 },
    { name: 'ShipCountry', label: 'Ship Country', sortable: true, width: 150 },
    { name: 'ShipRegion', label: 'Ship Region', sortable: true, width: 150 }
  ];

  constructor(
    private _dataTableService: TdDataTableService,
    private _orders: OrdersService,
    changeDetectorRef: ChangeDetectorRef,
    private datePipe: DatePipe,
    private router: Router
  ) {
    this.changeDetectorRef = changeDetectorRef;
   }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders() {
    this._orders.getOrders('/query/orders').subscribe((data: any) => {
      this.data = data.Results.map(this.formatOrder);
      this.filteredTotal = data.Results.length;
      this.filter();
    });
  }

  sort(sortEvent: ITdDataTableSortChangeEvent): void {
    this.sortBy = sortEvent.name;
    this.sortOrder = sortEvent.order;
    this.filter();
  }

  search(searchTerm: string): void {
    this.searchTerm = searchTerm;
    this.filter();
  }

  page(pagingEvent: IPageChangeEvent): void {
    this.fromRow = pagingEvent.fromRow;
    this.currentPage = pagingEvent.page;
    this.pageSize = pagingEvent.pageSize;
    this.filter();
  }

  filter(data?): void {
    let newData: any[] =  this.data;
    const excludedColumns: string[] = this.columns
    .filter((column: ITdDataTableColumn) => {
      return ((column.filter === undefined && column.hidden === true) ||
              (column.filter !== undefined && column.filter === false));
    }).map((column: ITdDataTableColumn) => {
      return column.name;
    });
    newData = this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
    this.filteredTotal = newData.length;
    newData = this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
    newData = this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
    this.filteredData = newData;
    this.changeDetectorRef.detectChanges();
  }

  private formatOrder = order => {
    const shippedDate = parseInt(order.ShippedDate.substr(6), 10);
    const orderDate = parseInt(order.OrderDate.substr(6), 10);
    order.ShippedDate = this.datePipe.transform(shippedDate, 'mediumDate');
    order.OrderDate = this.datePipe.transform(orderDate, 'mediumDate');
    return order;
  }

}
