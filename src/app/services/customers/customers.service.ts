import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { Customer } from './customers';
import { environment } from '../../../environments/environment';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  private handleError: HandleError;
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler
  ) {
    this.handleError = httpErrorHandler.createHandleError('CustomersService');
  }

  getCustomers (url): Observable<Customer[]> {
    return this.http.get<Customer[]>(`${this.apiUrl + url}`).pipe(
      catchError(this.handleError('getCustomers', []))
    );
  }

  customerDetails(url, id) {
    return this.http.get<Customer>(`${this.apiUrl + url}/${id}`).pipe(
      catchError(this.handleError('customerDetails', {}))
    );
  }
}
