export interface Customer {
    id: number;
    companyName: string;
    contactName: string;
    contactTitle: string;
    address: string;
    city: string;
    postalCode: string;
    country: string;
    phone: string;
    fax: string;
    region : string;
}