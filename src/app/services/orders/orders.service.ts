import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { Order } from './orders';
import { environment } from '../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private handleError: HandleError;
  apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler
  ) {
    this.handleError = httpErrorHandler.createHandleError('CustomersService');
  }

  /** GET customers from the server */
  getOrders (url): Observable<Order[]> {
    return this.http.get<Order[]>(this.apiUrl + url)
      .pipe(
        catchError(this.handleError('getCustomers', []))
      );
  }

  orderDetails(url, id, customerId) {
    return this.http.get<Order>(`${this.apiUrl + url}?OrderBy=${customerId},-${id}`)
      .pipe(
        catchError(this.handleError('orderDetails', []))
      );
  }
}
